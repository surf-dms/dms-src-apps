---

# - name: Update all the packages
#   ansible.builtin.yum: name=* state=3.4.3
#   when: ansible_os_family == "RedHat"


- name: install and enable the epel repo
  ansible.builtin.package:
    name:
      - epel-release
    state: present
  notify:
    - "Update the packages cache"
  tags: packages
  when: ansible_os_family == "RedHat"

- name: Set fact for Debian
  ansible.builtin.set_fact:
    openssldevl: "libssl-dev"
    postgresql_package: "postgresql"
    psycopg2: "python3-psycopg2"
  when: ansible_os_family == "Debian"

- name: Set fact for RedHat
  ansible.builtin.set_fact:
    openssldevl: "openssl-devel"
    postgresql_package: "postgresql-server"
    psycopg2: "python-psycopg2"
  when: ansible_os_family == "RedHat"


- name: Ensure bash, OpenSSl, and libssl are the latest versions
  ansible.builtin.package: name={{ item }} update_cache=yes
  become: true
  with_items:
    - bash
    - openssl
    - "{{ openssldevl }}"

- name: Ensure PostgreSQL database and dependencies are installed
  become: true
  ansible.builtin.package:
    name:
      - "{{ postgresql_package }}"
      - postgresql-contrib
      - "{{ psycopg2 }}"
    state: present

- name: Initialize PostgreSQL database
  become: true
  command: postgresql-setup initdb
    creates=/var/lib/pgsql/data/postgresql.conf
  when: ansible_os_family == "RedHat"


- name: Creates psql data directory
  become: true
  ansible.builtin.file:
    path: /var/lib/pgsql/data/
    state: directory
    group: postgres
    owner: postgres
    mode: o+rwx

  when: ansible_os_family == "Debian"

- name: Initialize PostgreSQL database
  become_user: postgres
  command: /usr/lib/postgresql/10/bin/initdb /var/lib/pgsql/data/
    creates=/var/lib/pgsql/data/postgresql.conf
  when: ansible_os_family == "Debian"

- name: Password protect loopback IPv4 connections
  become: true
  ansible.builtin.lineinfile:
    dest: /var/lib/pgsql/data/pg_hba.conf
    regexp: 'host\s+all\s+all\s+127.0.0.1/32'
    line: 'host all all 127.0.0.1/32 md5'
  notify: Restart PostgreSQL


- name: Password protect loopback IPv6 connections
  become: true
  ansible.builtin.lineinfile:
    dest: /var/lib/pgsql/data/pg_hba.conf
    regexp: 'host\s+all\s+all\s+::1/128'
    line: 'host all all ::1/128 md5'
  notify: Restart PostgreSQL


- name: Ensure PostgreSQL is configured
  become: true
  ansible.builtin.template:
    src: postgresql.conf.j2
    dest: "/var/lib/pgsql/data/postgresql.conf"
    owner: postgres
    group: postgres
    mode: 0600
  notify: Restart PostgreSQL


- name: Ensure PostgreSQL is enabled and started
  become: true
  ansible.builtin.service:
    name: postgresql
    enabled: true
    state: started

- name: Create iCAT database
  become_user: postgres
  become: true
  postgresql_db:
    name: "{{ irods_database_name }}" # this should be ICAT always.
    encoding: UTF-8
    lc_collate: en_US.UTF-8
    lc_ctype: en_US.UTF-8
    template: template0

- name: Create iRODS database user
  become_user: postgres
  become: true
  postgresql_user:
    db: "{{ irods_database_name }}"
    name: "{{ irods_database_user }}"
    password: "{{ irods_database_password }}"
    priv: ALL
